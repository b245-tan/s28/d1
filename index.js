// CRUD Operations
	// CRUD operation is the heart of any backend application
	// Mastering the CRUD operations is essential for any developer
	// This helps in building character and increasing exposure to logical statements that will help us manipulate
	// Mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.

// [SECTION] Create (Inserting document)
	// since MongoDB deals with object as its structure for documents, we can easily create them by providing objects into our methods.
	//  mongoDB shell also uses javascript for its syntax.

	// Inserting one document
	/*
		Syntax:
			db.collectionName.insertOne({object/document})
	*/
	
	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});


	// Inserting many documents
	/*
		Syntax:
			db.collectionName.insertMany([{objectA},{objectB}])
	*/

	db.users.insertMany([{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}, {
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"

	}]);

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		gender: "Female"
	});


// [SECTION] Find (Read)
	
	/*
		Syntax:
				// will show us all the documents in the collection 
			db.collectionName.find();

				// will show us all the documents in the collection 
			db.collectionName.find({field: value})
	*/

	db.users.find();

	db.users.find({age: 76});

	db.users.find({firstName: "Jane"});

		// finding documents using multiple fieldsets
	/*
		Syntax:
		db.collectionName.find({fieldA: valueA}, {fieldB: valueB});
	*/

	db.users.find({lastName: "Armstrong"}, {age: 82});


// [SECTION] Updating Documents

	// add document
	db.users.insertOne({
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		courses: [],
		deparment: "none"
	});

	// updateOne
	/*
		Syntax:
			db.collectionName.updateOne({criteria}, {$set: {field:value}});
	*/

	db.users.updateOne({
		firstName: "Jane"
	},{
		$set: {
			lastName: "Hawking";
		}
	});


	// updating multiple documents
	/*
		Syntax:
			db.collectionName.updateMany({criteria}, {$set: {
			field:value
			}
		});
	*/

	db.users.updateMany({
		firstName: "Jane"
	},{
		$set: {
			lastName: "Wick",
			age: 25
		}
	});


	db.users.updateMany({
		department: "none"
	},{
		$set: {
			department: "HR"
		}
	});

	
	// replacing the old document
	/*
		db.uses.replaceOne({criteria}, {document/objectToReplace});
	*/

	db.users.replaceOne({firstName: "Test"}, {
		firstName: "Chris",
		lastName: "Mortel"
	});


// [SECTION] Deleting documents

	// Deleting single document
	/*
		Syntax:
			db.collectionName.deleteOne({criteria});
	*/

	db.users.deleteOne({
		firstName: "Jane"
	});


	// Deleting many/multiple documents
	/*
		Syntax:
			db.collectionName.deleteMany({criteria});
	*/

	db.users.deleteMany({
		firstName: "Jane"
	});

	// reminder:
		// dont forget to place criteria when using delete many

// [SECTION] Advanced Querries 
	// embedded - nested object
	// nested field - array

	// query on an embedded document
	db.users.find({"contact.phone": "87654321"});

	// query an array with exact elements
	db.users.find({courses: ["React", "Laravel", "Sass"]});

	// query an array without regards to order and elements
	db.users.find({courses: {$all: ["React"]}});

	db.users.deleteMany({});